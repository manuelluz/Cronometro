﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Cronometro
{
    class Program
    {
        static void Main(string[] args)
        {
            var relogio = new Cronometro();

            Console.Write("Pressione ENTER para iniciar o cronometro");
            Console.ReadLine();

            relogio.StratClock();
            Console.WriteLine("Pressione ENTER novamente para desligar o cronometro");

            while (relogio.ClockState())
            {
                var tempo = DateTime.Now - relogio.StratTIme();
                Console.Write("\r Tempo Corrente:{0}", tempo);

                if (Console.KeyAvailable)
                {
                    if (Console.ReadKey().Key == ConsoleKey.Enter)
                    {
                        break;
                    }
                }
            }

            relogio.StopClock();

            Console.WriteLine("\r Tempo cronometrado: {0}", relogio.GetTimeSpan());
            Console.ReadLine();
        }
    }
}
