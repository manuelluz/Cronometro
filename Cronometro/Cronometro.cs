﻿namespace Cronometro
{
    using System;

    public class Cronometro
    {
        #region

        private DateTime _strat;

        private DateTime _stop;

        private bool _isrunning;

        #endregion

        public void StratClock()
        {
            if (_isrunning)
                throw new InvalidOperationException("Cronometro ligado!");

            _strat = DateTime.Now;
            _isrunning = true;
        }

        public void StopClock()
        {
            if (!_isrunning)
                throw new InvalidOperationException("Cronometro desligado!");

            _stop = DateTime.Now;
            _isrunning = false;
        }

        public TimeSpan GetTimeSpan()
        {
            return _stop - _strat;
        }

        public bool ClockState()
        {
            return _isrunning;
        }

        public DateTime StratTIme()
        {
            return _strat;
        }

    }
}
